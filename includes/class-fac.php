<?php

if (!defined('ABSPATH')) {
    exit;
}
if ($_REQUEST['form_update'] != 'ok') {
    if ($_REQUEST['code'] > 0) {
        ?>
            <div class="loading"><img src="https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif" alt="loading" /><br/>Un momento, por favor...</div>
            <script language="JavaScript"> 
            parent.location.href = window.location.href+"&form_update=ok";
            </script> 
    <?php 	die();
    }
}
    
/**
 * WC_Gateway_FirstAtlanticCommerceTilo class
 *
 * @extends WC_Payment_Gateway
 */
class WC_Gateway_FirstAtlanticCommerceTilo extends WC_Payment_Gateway
{

    /**
     * Constructor
     */
    public function __construct()
    {
        global $faccurrency;
        $this->id = 'factilo';
        $this->method_title = __('First Atlantic Commerce | TILOPAY', 'wc-gateway-fac-tilo');
        $this->method_description = __('First Atlantic Commerce | TILOPAY.', 'wc-gateway-fac-tilo');

        // Load the settings
        $this->init_settings();

        // User defined settings
        $this->title = $this->get_option('title');
        $this->fac_redirect = wc_get_checkout_url();
        $this->fac_key = $this->get_option('fac_key');
        $this->fac_enabled = $this->get_option('fac_enabled');
        $this->fac_user = $this->get_option('fac_user');
        $this->fac_password = $this->get_option('fac_password');
        $this->fac_capture = $this->get_option('fac_capture');
        $this->init_form_fields();
        // Hooks
        add_action('admin_notices', array($this, 'admin_notices'));
        add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
        if (isset($_REQUEST['form_update'])) {
            if ($_REQUEST['code'] > 0) {
				global $woocommerce;
				$order_id = $_REQUEST['order'];
				$customer_order = new WC_Order($order_id);
                $order = wc_get_order($order_id);
                if ($_REQUEST['code'] == "1") {                    
                    update_post_meta($order_id, 'fac_html_form', '');
                    update_post_meta($order_id, 'fac_tilo_auth_code', $_REQUEST['auth']);
					$order->add_order_note(__('Autorización : '.$_REQUEST['auth'], 'facTilo'));
					$order->add_order_note(__('Código : '.$_REQUEST['code'], 'facTilo'));
					$order->add_order_note(__('Descripción : '.$_REQUEST['description'], 'facTilo'));
                    if($this->fac_capture=="yes"){
						$order->payment_complete( $_REQUEST['auth'] );
                    }
                    $woocommerce->cart->empty_cart();
					wp_redirect($this->get_return_url($customer_order));

                } else {                    
					$order->add_order_note(__('Código : '.$_REQUEST['code'], 'facTilo'));
					$order->add_order_note(__('Descripción : '.$_REQUEST['description'], 'facTilo'));
                    $checkout_url = $woocommerce->cart->get_checkout_url(); 
                    header( 'Cache-Control: no-cache, must-revalidate' );
                    header( 'Location: ' . $checkout_url."?message_error=".$_REQUEST['description'], true, 307 );
                    exit;
                }
            }
        }
		
		if (isset($_REQUEST['message_error'])) {
			
			wc_add_notice(__($_REQUEST['message_error'], 'factilo'), 'error');
		}
    }

    /**
     * Notify of issues in wp-admin
     */
    public function admin_notices()
    {
        if ($this->enabled == 'no') {
            return;
        }


        // Check if enabled and force SSL is disabled
        if (get_option('woocommerce_force_ssl_checkout') == 'no') {
            echo '<div class="error"><p>' . sprintf(__('First Atlantic Commerce esta habilitado, pero el <a href="%s">forzar el pago seguro</a> esta deshabilitado; su checkout no esta seguro! Pro favor habilite SSL y asegure su servidor tenga un certificado SSL  - First Atlantic Commerce will solo trabajará en modo de pruebas.', 'woocommerce-gateway-fac-tilo'), admin_url('admin.php?page=wc-settings&tab=checkout')) . '</p></div>';
            return;
        }
    }


    /**
     * Logging method
     *
     * @param  string $message
     *
     * @return void
     */
    public function log($message)
    {
        if (empty($this->log)) {
            $this->log = new WC_Logger();
        }

        $this->log->add($this->id, $message);
    }

    /**
     * Check if the gateway is available for use
     *
     * @return bool
     */
    public function is_available()
    {
        $is_available = parent::is_available();

        // Only allow unencrypted connections when testing
        if (!is_ssl() and ! $this->testmode) {
            $is_available = false;
        }
        return true;
        return $is_available;
    }

    /**
     * get_icon function
     * Return icons for card brands supported.
     *
     * @since 2.3.0
     * @return string
     */
    public function get_icon()
    {
        $icons = $this->payment_icons();

        $icons_str = '';

        // $icons_str .= isset($icons['amex']) ? $icons['amex'] : '';
        $icons_str .= isset($icons['mastercard']) ? $icons['mastercard'] : '';
        $icons_str .= isset($icons['visa']) ? $icons['visa'] : '';
        $icons_str .= isset($icons['amex']) ? $icons['amex'] : '';
        

        return apply_filters('woocommerce_gateway_icon', $icons_str, $this->id);
    }
    /**
     * Initialise Gateway Settings Form Fields
     *
     */
    public function init_form_fields()
    {
        $fieldarr = array(
                'enabled' => array(
                    'title' => __('Habilitar/Deshabilitar', 'woocommerce-gateway-fac-tilo'),
                    'label' => __('Habilitar FAC', 'woocommerce-gateway-fac-tilo'),
                    'type' => 'checkbox',
                    'description' => '',
                    'default' => 'no'
                ),
                'title' => array(
                    'title' => __('Título', 'woocommerce-gateway-fac-tilo'),
                    'type' => 'text',
                    'description' => __('Título en método de pago.', 'woocommerce-gateway-fac-tilo'),
                    'default' => __('Pago tarjeta de crédito', 'woocommerce-gateway-fac-tilo')
                ),
                'fac_key' => array(
                    'title' => __('Key de la integración', 'woocommerce-gateway-fac-tilo'),
                    'type' => 'text',
                    'description' => __('Key de la integración facilitada por TILOPAY.', 'woocommerce-gateway-fac-tilo'),
                    'default' => __('Pago con tarjetas de crédito.', 'woocommerce-gateway-fac-tilo')
                ),
                'fac_user' => array(
                    'title' => __('Usuario del API', 'woocommerce-gateway-fac-tilo'),
                    'type' => 'text',
                    'description' => __('Usuario de conexión API TILOPAY.', 'woocommerce-gateway-fac-tilo'),
                    'default' => __('Pago con tarjetas de crédito.', 'woocommerce-gateway-fac-tilo')
                ),
                'fac_password' => array(
                    'title' => __('Password del API', 'woocommerce-gateway-fac-tilo'),
                    'type' => 'text',
                    'description' => __('Password de conexión API TILOPAY.', 'woocommerce-gateway-fac-tilo'),
                    'default' => __('Pago con tarjetas de crédito.', 'woocommerce-gateway-fac-tilo')
                ),
                'fac_capture' => array(
                    'title' => __('Capture', 'woocommerce-gateway-fac-tilo'),
                    'label' => __('Transacción inmediata', 'woocommerce-gateway-fac-tilo'),
                    'type' => 'checkbox',
                    'description' => __('Si no esta marcada se realiza solo una autorización. Fecha máxima para capturar (7 días después de autorizada), y luego de los 7 días cancelar orden automáticamente', 'woocommerce-gateway-fac-tilo'),
                    'default' => 'yes'
                ),
            );

        $this->form_fields = apply_filters('wc_factilo_settings', $fieldarr);
    }



    /**
     * Output payment fields
     *
     * @return      void
     */
    public function payment_fields()
    {
        //if (get_option('fac_tilo_active') == 'yes' && str_replace(array("https://","http://"),array("",""),get_option('fac_tilo_active_site')) == str_replace(array("https://","http://"),array("",""),site_url())):
        $this->credit_card_form();
        // endif;
    }

    /**
     * Process the payment and return the result
     *
     * @param int $order_id
     *
     * @return array
     */
    public function process_payment($order_id)
    {
        global $woocommerce;
        $order = wc_get_order($order_id);
        $headers = array(
                'Accept'     => 'application/json',
                'Content-Type' => 'application/json'
            );
        $datajson = [
                "email" => $this->fac_user,
                "password" => $this->fac_password
            ];
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://app.tilopay.com/api/v1/login");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datajson);
        $result = curl_exec($ch);
        if ($result === false || $result === null) {
            die('Curl failed: ' . curl_error($ch)); // TODO MANAGER ERROR
        }
        $result = json_decode($result);
        curl_close($ch);
        $headers = array(
                'Accept: application/json',
                'Content-Type: application/json',
                'Authorization: bearer '.$result->access_token
            );
        $order_total = $order->get_total();
        $datajson = [
                "redirect"=>$this->fac_redirect,
                "key"=> $this->fac_key,
                "amount"=>$order_total,
                "currency"=>get_woocommerce_currency(),
                "billToFirstName"=>$order->billing_first_name,
                "billToLastName"=>$order->billing_last_name,
                "billToAddress"=>$order->billing_address_1,
                "billToAddress2"=>$order->billing_address_2,
                "billToCity"=>$order->billing_city,
                "billToState"=>$order->billing_state,
                "billToZipPostCode"=>$order->billing_postcode,
                "billToCountry"=>$order->billing_country,
                "billToTelephone"=>$order->billing_phone,
                "billToEmail"=>$order->billing_email,
                "orderNumber"=>$order_id,
				"capture"=> $this->fac_capture == "yes" ? "1" : "0"
            ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://app.tilopay.com/api/v1/processPayment");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($datajson));
        $result = curl_exec($ch);
        if ($result === false || $result === null) {
            die('Curl failed: ' . curl_error($ch)); // TODO MANAGER ERROR
        }
        $result = json_decode($result);
        $html = $result->url;

        update_post_meta($order_id, 'fac_html_form', $html);
        update_post_meta($order_id, 'fac_capture', $this->fac_capture);

        $checkout_url = $woocommerce->cart->get_checkout_url();
        return array(	'result' => 'success',
                                'redirect' => $checkout_url.'?response_data=found&order_id='.$order_id
        );
    }
	
	/**
     * Refund Process
     */
	public function process_refund( $order_id ,$type) {
		$order = wc_get_order($order_id);
        $headers = array(
                'Accept'     => 'application/json',
                'Content-Type' => 'application/json'
            );
        $datajson = [
                "email" => $this->fac_user,
                "password" => $this->fac_password
            ];
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://app.tilopay.com/api/v1/login");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datajson);
        $result = curl_exec($ch);
        if ($result === false || $result === null) {
            die('Curl failed: ' . curl_error($ch)); // TODO MANAGER ERROR
        }
        $result = json_decode($result);
        curl_close($ch);
        $headers = array(
                'Accept: application/json',
                'Content-Type: application/json',
                'Authorization: bearer '.$result->access_token
            );
        $order_total = $order->get_total();
        $datajson = [
                "orderNumber"=>$order_id,
                "key"=> $this->fac_key,
                "amount"=>$order_total,
                "type"=> $type
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://app.tilopay.com/api/v1/processModification");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($datajson));
        $result = curl_exec($ch);
        if ($result === false || $result === null) {
            die('Curl failed: ' . curl_error($ch)); // TODO MANAGER ERROR
        }
		        echo $result;
        $result = json_decode($result);
        if($result->ReasonCodeDescription == "Transaction successful"){
            update_post_meta($order_id, 'fac_capture', "yes");
        }
		$order->add_order_note(__('Resultado de FAC : '.$result->ReasonCodeDescription, 'facTilo'));

    }


        /**
	 * All payment icons that work with FAC. Some icons references
	 * WC core icons.
	 *
	 * @since 2.3.0
	 * @return array
	 */
	public function payment_icons() {
		return apply_filters(
			'wc_fac_payment_icons',
			array(
				'visa'       => '<img src="' . WC_FAC_PLUGIN_URL . '/assets/images/visa.svg" style="float: none; max-height: 20px; margin:0px 10px" alt="Visa" />',
				'amex'       => '<img src="' . WC_FAC_PLUGIN_URL . '/assets/images/amex.svg" style="float: none; max-height: 20px; margin:0px 10px"  alt="Ame" />',
				'mastercard' => '<img src="' . WC_FAC_PLUGIN_URL . '/assets/images/mastercard.svg" style="float: none; max-height: 20px; margin:0px 10px" alt="Mastercard" />',
			)
		);
	}
}
