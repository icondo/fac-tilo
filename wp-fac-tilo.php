<?php

/*
 * Plugin Name: First Atlantic Commerce Gateway TILO BAC
 * Plugin URI: http://http://tilo.co//
 * Description: First Atlantic Commerce Gateway Description
 * Version: 1.0
 * Author:  Tilo
 * Author URI: http://http://tilo.co//
 * License:
 * License URI: http://http://tilo.co//
 */

if (!defined('ABSPATH')) {
    exit;
}

$plugin_slug = basename(dirname(__FILE__));

function woocommerce_init_factilo_gateway()
{
    if (!class_exists('WC_Payment_Gateway')) {
        return;
    }

    global $plugin_slug, $wp_version;
    // Includes
    
    define('PLUGINtiloURL', untrailingslashit(plugins_url(basename(plugin_dir_path(__FILE__)), basename(__FILE__))));
    define('PLUGINtiloPRODUCTID', 'Tilopay | FAC');
    define('PLUGINtiloPRODUCTSLUG', $plugin_slug);
    define('PLUGINtiloVERSION', $plugin_slug . ' ' . $plugin_slug . '.php');
    define('PLUGINtiloCURRENTVERSION', $plugin_slug . '/'.$plugin_slug.'.php');
    define('PLUGINtiloSOFTVERSION', '1.0');
    define('WC_FAC_PLUGIN_URL', untrailingslashit(plugins_url(basename(plugin_dir_path(__FILE__)), basename(__FILE__))));

    include_once('includes/class-fac.php');



    // Register the gateway in WC
    function woocommerce_register_factilo_gateway($methods)
    {
        $methods[] = 'WC_Gateway_FirstAtlanticCommerceTilo';

        return $methods;
    }

    add_filter('woocommerce_payment_gateways', 'woocommerce_register_factilo_gateway');
    // define the woocommerce_credit_card_form_fields callback

    function filter_woocommerce_credit_card_form_fields()
    {
        if (isset($_REQUEST['response_data'])) {
            if ($_REQUEST['response_data'] == 'found') {
                $order_id = $_REQUEST['order_id'];
                $html = get_post_meta($order_id, 'fac_html_form');
                echo '<!-- Remember to include jQuery :) -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
            <style>
			.blocker {
			z-index: 9999 !important;
			}

			.modal{
				width: 94% !important;
				margin: 0 auto !important;
				left: 3% !important;
			}
			.modal .row {
				margin-right: 0 !important;
				margin-left: 0 !important;
				overflow: hidden !important;
			}
			embed{
			overflow: hidden !important;
			min-width: 100% !important;
			background-color : #fff!important;
			}
			@media (min-width: 992px) {
				.modal {
					width: 376px !important;
				}
			}
			.modal{
				width: 94% !important;
				margin: 0 auto !important;
				left: 3% !important;
				padding: 15px 0 !important;
			}
			.modal embed > .row {
				margin-right: 0 !important;
				margin-left: 0 !important;
				overflow: hidden !important;
			}
			embed{
				overflow: hidden !important;
				min-width: 100% !important;
			}

			embed .row{
				margin-right: 0 !important;
				margin-left: 0 !important;
			}
			@media (min-width: 992px) {
				.modal {
					width: 376px !important;
				}
			}
			</style>
            <!-- jQuery Modal -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
            <script>
            $( document ).ready(function() {
              $("#modal1").modal({
				  escapeClose: false,
				  clickClose: false,
				  showClose: false
				})
            });
          </script> 
            <div id="modal1" class="modal" data-backdrop="static" data-keyboard="false">
            <embed src="'.$html[0].'" style="height: 700px; overflow:auto; margin: 0 auto;">
            </div>
            <!-- Link to open the modal -->
            <p><a href="#ex1" class="btn modal-trigger" style="display:none" id="modalClick" rel="modal:open">Open Modal</a></p>
            ';
            }
        }
    }
    add_filter('woocommerce_after_checkout_form', 'filter_woocommerce_credit_card_form_fields');

    function woocommerce_factilo_process_payment($order_id)
    {
        $fac = new WC_Gateway_FirstAtlanticCommerceTilo;

        $fac->process_payment($order_id);
    }
/*
    function woocommerce_factilo_process_refund($order_id)
    {
        $fac = new WC_Gateway_FirstAtlanticCommerceTilo;

        $fac->process_refund($order_id);
    }
    // define the woocommerce_order_refunded callback
    function action_woocommerce_order_refunded($order_get_id, $refund_get_id)
    {
        $order_id = $order_get_id;
        $fac = new WC_Gateway_FirstAtlanticCommerceTilo;
        $fac->process_refund($order_id);
    };
    
    function fac_save_global_notice_meta_box_data()
    {
        if ($_POST["post_red"]=="reembolso") {
            $fac = new WC_Gateway_FirstAtlanticCommerceTilo;
            $fac->process_refund($_POST["orderId"]);
        }
    }
*/
    function fac_add_meta_boxes()
    {
        $fac = new WC_Gateway_FirstAtlanticCommerceTilo;
        if ($fac->fac_capture=="no") {
            add_meta_box('mv_other_fields', __('Capturar', 'woocommerce'), 'add_fac_fields', 'shop_order', 'side', 'core');
        }
    }
    function add_fac_fields()
    {
        global $post; ?>
		<form action="<?php echo admin_url('admin-post.php'); ?>" method="post"  id="moovin-form-id" >
			<input type="hidden" name="orderId" id="orderId" value="<?php echo $post->ID; ?>">
			<input type="hidden" name="post_red" id="post_red" value="reembolso">
			<button class="button button-primary" type="submit" name="create_order_fac" value="create_order_fac">Capturar</button>
		</form>
		<?php
    }
    


    function fac_order_status_changed($order_id)
    {
        if (! $order_id) {
            return;
        }
        $order = wc_get_order( $order_id );
        $capture = get_post_meta($order_id,'fac_capture')[0];
        if ($order->data['status'] == 'processing' && $capture == "no") {
            $fac = new WC_Gateway_FirstAtlanticCommerceTilo;
            $fac->process_refund($order_id, "1");
        }
        if ($order->data['status'] == 'refunded' && $capture == "yes") {
            $fac = new WC_Gateway_FirstAtlanticCommerceTilo;
            $fac->process_refund($order_id, "2");
        }
    }
    /**
     * Load admin scripts.
     *
     * @since 2.3.1
     * @version 2.3.1
     */
    function admin_scripts()
    {
        wp_enqueue_script('woocommerce_fac_js', plugins_url('assets/js/checkout.js'), array());
    }

// front end
function custom_front_scripts() {
	wp_enqueue_style( 'fac_styles', plugins_url('/assets/css/admin.css', __FILE__), array(), null, 'all' );
}

add_action('wp_enqueue_scripts', 'custom_front_scripts' );

    // Actions to capture or void authorized transactions
    add_action('admin_enqueue_scripts', 'admin_scripts');
    //add_action('woocommerce_order_status_on-hold_to_processing', 'woocommerce_factilo_process_payment');
    //add_action('woocommerce_order_status_on-hold_to_completed', 'woocommerce_factilo_process_payment');
    //add_action('woocommerce_order_refunded', 'action_woocommerce_order_refunded', 10, 2);
    add_action('woocommerce_order_status_changed', 'fac_order_status_changed');
    //add_action( 'save_post', 'fac_save_global_notice_meta_box_data' );
    //add_action('add_meta_boxes', 'fac_add_meta_boxes');





    /*
    add_action('woocommerce_order_status_on-hold_to_cancelled', 'woocommerce_factilo_process_refund');
    add_action('woocommerce_order_status_on-hold_to_refunded', 'woocommerce_factilo_process_refund');
    */
}
add_action('plugins_loaded', 'woocommerce_init_factilo_gateway', 0);
