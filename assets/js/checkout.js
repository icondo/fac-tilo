jQuery(function ($) {
    'use strict';

	/**
	 * Object to handle Greenpay admin functions.
	 */
    var fac_admin = {

		/**
		 * Initialize.
		 */
        init: function () {
        }
    };
    fac_admin.init();
});
